import org.apache.commons.io.FileUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileServlet extends HttpServlet 
{
    public void init(ServletConfig servletConfig) throws ServletException {
    }

    public ServletConfig getServletConfig() {
        return null;
    }

    public void service(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException 
    {
    	String sObjectName = servletRequest.getParameter( "name" );

    	String sType = servletRequest.getParameter( "type" );
    	if ( sType == null )
    		sType = "carto";
    	
    	int nMinVersion = 0;
    	try
    	{
        	String sMinVersion = servletRequest.getParameter( "minversion" );
        	if ( sMinVersion != null )
        		nMinVersion = Integer.parseInt( sMinVersion );
    	}
    	catch ( NumberFormatException ex )
    	{
    		; // nothing to do ... keep 0 as value
    	}
    	
    	System.out.println( "Request for object name=" + sObjectName + " / type=" + sType + " / minVersion=" + nMinVersion );

		boolean shouldReturnEmptyOnFail = servletRequest.getParameter( "returnemptyonfail" ) != null;
		
		Pattern patternObject = Pattern.compile( "^" + sObjectName + "-" + sType + "-(\\d+)\\.[^\\.]+$" );
		
    	int nVersionHighest = 0;
    	File fHighest = null;

    	File fDir = new File( "res" );
        File[] arfFiles = fDir.listFiles();
        for( File f : arfFiles )
        {
            if ( f.isFile() )
            {
            	Matcher m = patternObject.matcher( f.getName() );
            	if ( m.matches() )
            	{
           			int nVersion = Integer.parseInt( m.group( 1 ) );
           			if( nVersion > nVersionHighest )
           			{
           				nVersionHighest = nVersion;
           				fHighest = f;
           			}
            	}
            }
        }
        
        if ( fHighest != null  &&  nVersionHighest > nMinVersion )
        {
            // servletResponse.setStatus(HttpServletResponse.SC_OK);
    		servletResponse.setContentType("application/octet-stream");
    		long lgFile = fHighest.length();
            servletResponse.setContentLength( (int) lgFile );
            ServletOutputStream os = servletResponse.getOutputStream();
            FileUtils.copyFile( fHighest, os );
        	System.out.println( "... returned file " + fHighest.getName() + " / " + fHighest.length() + " bytes" );
        }
        else if ( shouldReturnEmptyOnFail )
        {
        	// not found, return an empty result
    		servletResponse.setContentType("application/octet-stream");
            servletResponse.setContentLength( 0 );
        	System.out.println( "... returned empty result" );
        }
        else
        {
        	// not found, return a 404
        	servletResponse.setStatus( HttpServletResponse.SC_NOT_FOUND );
        	System.out.println( "... returned 404" );
        }
    }

}
