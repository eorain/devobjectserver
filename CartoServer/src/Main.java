import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

public class Main {

    public static String m_sDir = "."; // "epix/epix/_Portraits"; // "T:\\epix\\epix\\_Portraits";
    // public static String m_sDirAllHTML = "epix/epix/_Portraits"; // "T:/epix/epix/_Portraits";
    public static String m_sDirSel = "sel";

    public static void main(String[] args ) {
        try
        {
            Server server = new Server(8080);
            Context root = new Context(server,"/", Context.SESSIONS);
            root.addServlet(new ServletHolder( new FileServlet() ), "/getobjectdata2");
            server.start();
        }
        catch( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
